import React, { useState } from 'react';
import { Spinner, Modal, Button, FormControl } from 'react-bootstrap';
import { upcoverimg } from '../Modal-avatar/action';
import { useDispatch } from 'react-redux';
export const ModalImageCover = (props) => {
    const [show, setShow] = useState(false);
    const [file, setfile] = useState('');
    // const handleClose = () => setShow(false);
    // const handleShow = () => setShow(true);

    const dispatch = useDispatch();
  
    let formimagecover = new FormData();
    formimagecover.append('single_image_training',file);
    const upload = () =>{
      if(file){
        dispatch(upcoverimg(formimagecover))
        props.onHide()
      }
       
    }
    return (
        
        <Modal
          {...props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Upload Image Cover
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormControl
              type="file"
              onChange={(e) => setfile(e.target.files[0])}
            />
          </Modal.Body>
          {/* {loadingpage.loadpage === true?  <Spinner animation="border" variant="secondary" />:""} */}
          <Modal.Footer>
            <Button onClick={upload}>Upload</Button>
            <Button onClick={props.onHide}>close</Button>
          </Modal.Footer>
        </Modal>
      )
}