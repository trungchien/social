import { Action_Type_Getuserid } from "./action";


const userid = {
    listuserid: [],
    loadpage: false 
}
const getusersID = (state = userid, action) => {
    switch (action.type) {
        case Action_Type_Getuserid.GET_USERID:{
            return {...state, loadpage: true}
        }
        case Action_Type_Getuserid.GET_USERID_SUCCESS:{
          
            return {...state, listuserid: action.payload,loadpage: false}
        }  
        case Action_Type_Getuserid.GET_USERID_ERROR:{
            return {...state}
        }  
    
        default:
            return{
                ...state
            };
    }
};
export default getusersID;