export const Action_Type_Getuserid = {
    GET_USERID: 'GET_USERID',
    GET_USERID_SUCCESS: 'GET_USERID_SUCCESS',
    GET_USERID_ERROR: 'GET_USERID_ERROR',

    UPDATEASTORY: 'UPDATEASTORY',
    UPDATEASTORY_SUCCESS: 'UPDATEASTORY_SUCCESS',
    UPDATEASTORY_ERROR: 'UPDATEASTORY_ERROR',

    UPDATE_INFOR: 'UPDATE_INFOR',
    UPDATE_INFOR_SUCCESS: 'UPDATE_INFOR_SUCCESS',
    UPDATE_INFOR_ERROR: 'UPDATE_INFOR_ERROR'
}
export const getuserid = (list) => {
    return {
        type: Action_Type_Getuserid.GET_USERID,
        payload: list
    }
}

export const getuseridsuccess = (list) => {
    return {
        type: Action_Type_Getuserid.GET_USERID_SUCCESS,
        payload: list
    }
}

export const getuseriderror = (list) => {
    return {
        type: Action_Type_Getuserid.GET_USERID_ERROR,
        payload: list
    }
}

export const updateStory = (list) => {
    return {
        type: Action_Type_Getuserid.UPDATEASTORY,
        payload: list
    }
}

export const updateStorySuccess = (list) => {
    return {
        type: Action_Type_Getuserid.UPDATEASTORY_SUCCESS,
        payload: list
    }
}

export const updateStoryError = (list) => {
    return {
        type: Action_Type_Getuserid.UPDATEASTORY_ERROR,
        payload: list
    }
}

export const updateInfor = (list) => {
    return {
        type: Action_Type_Getuserid.UPDATE_INFOR,
        payload: list
    }
}

export const updateInforsuccess = (list) => {
    return {
        type: Action_Type_Getuserid.UPDATE_INFOR_SUCCESS,
        payload: list
    }
}

export const updateInforerror = (list) => {
    return {
        type: Action_Type_Getuserid.UPDATE_INFOR_ERROR,
        payload: list
    }
}