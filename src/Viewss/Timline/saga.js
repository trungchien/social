import { Action_Type_Getuserid, getuseriderror, getuseridsuccess, updateStorySuccess, getuserid } from "./action";
import { put, takeLatest } from "redux-saga/effects";

function* Sagagetuserid(userid){
    const token = localStorage.getItem('islogin');
    
    if(userid)
    try {
     const reqgetuserid = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/get-user?user_id=${userid.payload}`, {
         method: "GET",
         headers: new Headers({
             'Content-Type': 'application/json',
             Accept: 'application/json',
             'Authorization' : `Bearer ${token}`
         })
 
     })
     const resgetUserid = yield reqgetuserid.json();
     console.log(resgetUserid);
     if(resgetUserid.state ===true){
        
         yield put(getuseridsuccess(resgetUserid.data));
     }
    } catch (error) {
        yield put(getuseriderror(error))
    }
 }
 function* SagaUpdateStory(story){
     const token = localStorage.islogin;
     const id = localStorage.id
    console.log(story);
    try {
        const reqUpStory = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/update-story/${id}`,{
            method: 'PUT',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Authorization': `Bearer ${token}`
            }),
             body: JSON.stringify({story: story.payload})
        })
        const resUpStory = yield reqUpStory.json();
        if(resUpStory.state === true){
            yield put(getuserid(id))
        }
        console.log(resUpStory);
    } catch (error) {
        
    }
 }
 function * SagaUpdateInfor(infor) {
    const token = localStorage.islogin;
    const id = localStorage.id
    console.log(infor);
    try {
        const reqUpInfor = yield fetch(`http://aht-social.herokuapp.com/api/v1/user/update/${id}`,{
            method: 'PUT',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Authorization': `Bearer ${token}`
            }),
             body: JSON.stringify(infor.payload.valueEditProfile)
        })
        const resUpInfor = yield reqUpInfor.json();
        if(resUpInfor.state === true){
            yield put(getuserid(id))
        }
        console.log(resUpInfor);
       
    } catch (error) {
        
    }
 }
 export default function* watchSagauserid() {
     yield takeLatest(Action_Type_Getuserid.GET_USERID, Sagagetuserid)
     yield takeLatest(Action_Type_Getuserid.UPDATEASTORY, SagaUpdateStory)
     yield takeLatest(Action_Type_Getuserid.UPDATE_INFOR, SagaUpdateInfor)
 }