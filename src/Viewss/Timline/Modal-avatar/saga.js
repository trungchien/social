import { takeLatest, call, put } from "redux-saga/effects";
import { ActionUploadavt, updateavatar, updatecoverimg } from "./action";
import axios from "axios";
import { useDispatch } from "react-redux";
import { getuserid } from "../action";
const qs = require('querystring')
function* SagaUploadAvatar(user) {
    const token = localStorage.getItem('islogin');
 
    const res = yield axios.post(`https://brand.spify.io/r3st/api/v1/upload-single-image`, user.payload, {


        headers: {
            'Content-Type': 'multipart/form-data',
            'Accept': '*/*',
            'Access-Control-Allow-Origin': '*/*',
            'Authorization': `Bearer ${token}`
        },
    })
    console.log(res);
    if (res.data.status === true) {
        // yield put(getuserid(user.payload.id))
        yield put(updateavatar(res.data.location))
    }



}
function* SagaUploadImageCover(user) {
    const token = localStorage.getItem('islogin');
    
    const res = yield axios.post(`https://brand.spify.io/r3st/api/v1/upload-single-image`, user.payload, {


        headers: {
            'Content-Type': 'multipart/form-data',
            'Accept': '*/*',
            'Access-Control-Allow-Origin': '*/*',
            'Authorization': `Bearer ${token}`
        },
    })
    console.log(res);
    if (res.data.status === true) {
        // yield put(getuserid(user.payload.id))
        yield put(updatecoverimg(res.data.location))
    }



}
function* SagaUpdateAvatar(location) {
    console.log(location.payload);
   
    const token = localStorage.getItem('islogin');
    const id = localStorage.getItem('id');
    const res = yield axios.put(`https://social-aht.herokuapp.com/api/v1/user/update-avatar/${id}`, qs.stringify({location: location.payload}), {


        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': '*/*',
            'Authorization': `Bearer ${token}`
        },
      
    })
    console.log(res);
    if (res.status === 200) {
        yield put(getuserid(id))

    }



}
function* SagaUpdateImageCover(location) {
    const token = localStorage.getItem('islogin');
    const id = localStorage.getItem('id');
    const reqImagecover = yield axios.put(`https://social-aht.herokuapp.com/api/v1/user/update-cover-image/${id}`, qs.stringify({location: location.payload}), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': '*/*',
            'Access-Control-Allow-Origin': '*/*',
            'Authorization': `Bearer ${token}`
        },
    })
    if (reqImagecover.status === 200) {
        yield put(getuserid(id))
    }

}
export default function* watchSagaUploadAvt() {
    yield takeLatest(ActionUploadavt.AVATAR,SagaUploadAvatar)
    yield takeLatest(ActionUploadavt.IMAGE_COVER,SagaUploadImageCover)
    yield takeLatest(ActionUploadavt.UPDATE_AVATAR, SagaUpdateAvatar)
    yield takeLatest(ActionUploadavt.UPDATE_IMAGE_COVER, SagaUpdateImageCover)

}