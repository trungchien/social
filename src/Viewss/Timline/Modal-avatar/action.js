export const ActionUploadavt = {
    AVATAR: 'AVATAR',
    AVATAR_SUCCESS: 'AVATAR_SUCCESS',
    AVATAR_ERROR: 'AVATAR_ERROR',
    
    UPDATE_AVATAR: 'UPDATE_AVATAR',
    UPDATE_AVATAR_SUCCESS: 'UPDATE_AVATAR_SUCCESS',
    UPDATE_AVATAR_ERROR: 'UPDATE_AVATAR_ERROR',

    IMAGE_COVER: 'IMAGE_COVER',
    IMAGE_COVER_SUCCSESS: 'IMAGE_COVER_SUCCESS',
    IMAGE_COVER_ERROR: 'IMAGAE_COVER_ERROR',
  
    UPDATE_IMAGE_COVER : 'UPDATE_IMAGE_COVER',
    UPDATE_IMAGE_COVER_SUCCESS: 'UPDATE_IMAGE_COVER_SUCCESS',
    UPDATE_IMAGE_COVER_ERROR: 'UPDATE_IMAGE_COVER_ERROR'

}

export const upavatar = (list) => {
    return{
        type: ActionUploadavt.AVATAR,
        payload: list
    }
}

export const upavatarsuccess = (list) => {
    return {
        type: ActionUploadavt.AVATAR_SUCCESS,
        payload: list
    }
}

export const upavatarerror = (list) => {
    return {
        type: ActionUploadavt.AVATAR_ERROR,
        payload: list
    }
}
 
export const updateavatar = (list) => {
    return{
        type: ActionUploadavt.UPDATE_AVATAR,
        payload: list
    }
}

export const updateavatarsuccess = (list) => {
    return{
        type: ActionUploadavt.UPDATE_AVATAR_SUCCESS,
        payload: list
    }
}

export const updateavatarerror = (list) => {
    return{
        type: ActionUploadavt.UPDATE_AVATAR_ERROR,
        payload: list
    }
}

export const upcoverimg = (list) => {
    return {
        type: ActionUploadavt.IMAGE_COVER,
        payload: list
    }
}

export const upcoverimgsuccess = (list) => {
    return {
        type: ActionUploadavt.IMAGE_COVER_SUCCSESS,
        payload: list
    }
}

export const upcoverimgerror = (list) => {
    return {
        type: ActionUploadavt.IMAGE_COVER_ERROR,
        payload: list
    }
}

export const updatecoverimg = (list) => {
    return {
        type: ActionUploadavt.UPDATE_IMAGE_COVER,
        payload: list
    }
}

export const updatecoverimgsuccess = (list) => {
    return {
        type: ActionUploadavt.UPDATE_IMAGE_COVER_SUCCESS,
        payload: list
    }
}

export const updatecoverimgerror = (list) => {
    return {
        type: ActionUploadavt.UPDATE_IMAGE_COVER_ERROR,
        payload: list
    }
}