import React, { useState } from 'react';
import { Modal, Button, FormControl, Spinner } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { upavatar } from './action';
import axios from 'axios';
import { getuserid } from '../action';
export default function ModalAvatar(props) {
 
  const [file, setfile] = useState('');

  // console.log(file);
  const dispatch = useDispatch();
  // console.log(props.userid);
  const loadingpage = useSelector(state => state.getusersID)
  const upload = () => {
    
    // console.log(file);
    let formdata = new FormData();
    formdata.append("single_image_training",file)
    if(file) {
     dispatch(upavatar(formdata))
     props.onHide();
    }
    
  }
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title-vcenter">
          Upload Avatar
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <FormControl
          type="file"
          onChange={(e) => setfile(e.target.files[0])}
        />
      </Modal.Body>
      {loadingpage.loadpage === true?  <Spinner animation="border" variant="secondary" />:""}
      <Modal.Footer>
        <Button onClick={upload}>Upload</Button>
        <Button onClick={props.onHide}>close</Button>
      </Modal.Footer>
    </Modal>
  )
}