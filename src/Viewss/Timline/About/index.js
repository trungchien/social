import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import "../About/style.scss"
import { updateInfor } from '../action';
export default function About(props) {
    const [show, setShow] = useState(false);
    // const [listss,setlist] = useState(props.data);
    const dispatch = useDispatch();
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    let query = new URLSearchParams(useLocation().search);
    const userid = query.get("userid")
    const lists = useSelector(state => state.getusersID.listuserid);
  

    const [infor, setinfor] = useState({
        email: props.data.email,
        birthday: props.data.birthday,
        gender: props.data.gender,
        address: props.data.address,
        phone: props.data.phone,
        relationship: props.data.relationship
    });
   console.log('s',infor);
useEffect(()=>{
    setinfor(props.data)
},[props.data]);
const handleOnchange = (key) =>(event) =>{
    setinfor({...infor,[key]: event.target.value})
}
const valueEditProfile = {
    email: infor.email,
    phone: infor.phone,
    birthday: infor.birthday,
    address: infor.address,
    gender: infor.gender,
    relationship: infor.relationship ,
  };
const handleSubmit=()=>{
     if(valueEditProfile){
          dispatch(updateInfor({valueEditProfile}))
          handleClose()
     }
}
var infordetail = "";
    if (lists) {
        infordetail = (
            <div>
                <Table className="mt-3 table-infor" size="md">
                    <tbody>
                        <tr>
                            <td><i className="fas fa-birthday-cake"></i> Birthday:</td>
                            <td>{lists.birthday}</td>
                        </tr>
                        <tr>
                            <td><i className="fas fa-map-marker-alt"></i> Address:</td>
                            <td>{lists.address}</td>

                        </tr>
                        <tr>
                            <td><i className="fas fa-genderless"></i> Gender:</td>
                            <td >{lists.gender === 1 ? "Male" : "Female"}</td>
                        </tr>
                        <tr>
                            <td><i className="far fa-envelope"></i> Email:</td>
                            <td >{lists.email}</td>
                        </tr>
                        <tr>
                            <td><i className="fas fa-phone"></i> Phone:</td>
                            <td >{lists.phone}</td>
                        </tr>
                        <tr>
                            <td><i className="far fa-heart"></i> Marital Status: </td>
                            <td>{lists.relationship === 0 ? "Độc thân" : lists.relationship === 1 ? "Đã kết hôn" : lists.relationship === 2 ? "Quan hệ phức tạp" : ""}</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        )
    }
    else infordetail = <></>
    return (
        <div className="pt-3">
            <h3>Information Details</h3>
            {infordetail}
            <Button variant="primary mt-3" onClick={handleShow}>
                 Chỉnh sửa chi tiết
            </Button>
            <Modal show={show} onHide={handleClose} size="lg">
                <Modal.Header closeButton>
                    <Modal.Title>Edit Infor Details</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group >
                            <Form.Label>Birthday</Form.Label>
                            <Form.Control type="date" value={infor.birthday} onChange={handleOnchange("birthday")} placeholder="Birthday" />
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Address</Form.Label>
                            <Form.Control type="text" value={infor.address} onChange={handleOnchange("address")} placeholder="Address" />
                        </Form.Group> 
                        <Form.Group className="d-flex">
                            <Form.Label>Gender</Form.Label>
                            <Form.Check className="mr-2 ml-3" value="1" onChange={handleOnchange("gender")} type="radio" label="Male" name="gender"/>
                            <Form.Check type="radio" value="0" onChange={handleOnchange("gender")} label="Female"  name="gender"/>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Email</Form.Label>
                             <Form.Control type="text" value={infor.email} onChange={handleOnchange("email")} placeholder="Email" />
                         </Form.Group>
                         <Form.Group>
                             <Form.Label>Phone</Form.Label>
                             <Form.Control type="text" value={infor.phone} onChange={handleOnchange("phone")} placeholder="Phone" />
                         </Form.Group>
                        <Form.Group>
                            <Form.Label>Marital status</Form.Label>
                            <div className="d-flex">
                            <Form.Check className="mr-2" value="0"  onChange={handleOnchange("relationship")} type="radio" label="Single" name="marital"/>
                            <Form.Check className="mr-2" type="radio" value="1"  onChange={handleOnchange("relationship")} label="Married"  name="marital"/>
                            <Form.Check type="radio" label="Complex" value="2"  onChange={handleOnchange("relationship")}  name="marital"/>
                            </div>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleSubmit}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>

    )
}