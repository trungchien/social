import React, { useEffect, useState } from 'react';
import { useLocation, Link, Route, useHistory, Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import Header from '../Header';
import '../Timline/style.scss'
import { getuserid, updateStory } from './action';

import Switch from 'react-bootstrap/esm/Switch';
import PostDetail from '../Home/PostDetail';
import { Button, Spinner, FormControl } from 'react-bootstrap';
import ModalAvatar from './Modal-avatar';
import { ModalImageCover } from './Modal-ImageCover';
import About from './About';
import { follow, unfollow } from '../Home/Follow/action';
export default function Timeline() {
    const [modalShow, setModalShow] = React.useState(false);
    const [showcoverImg, setshowcoverImg] = React.useState(false);

    const [checkstory, setCheckStory] = useState({ isStory: false })
    const data = useSelector(state => state.getusersID.listuserid);
    console.log(data);
    const idaccount = localStorage.getItem('id');
    console.log(idaccount);
    const followresult = [{...data}];
    const followresult22 = [data.followers];
    const result = followresult22[0];
    console.log(result);
    console.log(followresult22?.includes(idaccount)===true?'hi':'his');
    console.log(followresult);  
    const idSearch = followresult.map(item => item.followers)
  
    
   




    const [story, setStory] = useState('')
    const loadingpage = useSelector(state => state.getusersID)

    const history = useHistory();
    const AboutDetail = () => {
        history.push(`/timeline/about?userid=${data._id}`);

    }
    var datarender = "";
    var StoryUser = <p className="infor-user__story">{data.story}</p>
    var InputStory = <FormControl className="mt-3"
        type="text"
        value={story}
        onChange={e => setStory(e.target.value)}
    />
    var urlImagecover = <img src={data.coverImage !== "" ? data.coverImage : "/images/covers/1.jpg"} />

    var inforUser = <div className="infor-user infordetail mt-3">
        <p><i className="fas fa-home"></i> Sống tại <span className="profile-link font-weight-bold">{data.address}</span></p>
        <p><i className="fab fa-stack-overflow"></i> Có <span className="profile-link font-weight-bold">2</span> người theo dõi</p>
        <p><i className="far fa-heart"></i> {data.relationship === 0 ? "Độc thân" : data.relationship === 1 ? "Đã kết hôn" : data.relationship === 2 ? "Quan hệ phức tạp" : ""}</p>
        <Button variant="light" className="infor-user__btn_edit" onClick={AboutDetail}>Xem chi tiết</Button>
    </div>

    if (data)

        datarender = (
            <>

                <a href="#"> <img src={data.avatar !== "" ? data.avatar : data.avatar === "" && data.gender === 1 ? "/images/users/male.jpg" : "/images/users/female.jpg"} className="img-responsive profile-photo" /></a>
                <h3 className="timeline-fullname">{data.firstName} {data.lastName}</h3>
                <p className="text-muted">Creative Director</p>
            </>
        )
    else datarender = <></>


    let query = new URLSearchParams(useLocation().search);
    const userid = query.get("userid")

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getuserid(userid))
    }, [])

    // var buttonAddfriend =''; 
    // if(followresult){
    //     buttonAddfriend = <Link type="button" to={`/timeline?userid=${followresult._id}`}
    //     onClick={isFollow=== true ? (() => removeFollow(followresult._id)) : (() => addFollow(followresult._id))}>
    //     <i className={`mr-2 ${isFollow === true ? "fas fa-wifi" : "fas fa-user-plus"}`}></i>
    //     {isFollow === true ? "Đang follow" : "Follow"}
    // </Link>
    // }

    // else 
    // buttonAddfriend=<></>
    // FOLLOW
    const addFollow = (id,index) => {
        console.log(id);
        if (id) {
            dispatch(follow(id))

        }
        result.push(idaccount);
    }
    const removeFollow = (id,index) => {
        if (id) {
            dispatch(unfollow(id))
        }
        result.splice(result.indexOf(idaccount), 1);
        // result.filter(item => item === idaccount)
    }
    // ENDFOLLOW

    const handleEditStory = () => {
        setCheckStory({ isStory: true })
    }
    const handleExit = () => {
        setCheckStory({ isStory: false })
    }
    const SubmitEditStory = () => {
        console.log(story)
        if (story) {

            dispatch(updateStory(story))
            setCheckStory({ isStory: false })
        }

    }
   
    return (
        <div className={loadingpage.loadpage === true ? "page-hidden" : "timeline"}>

            <Header />
            {loadingpage.loadpage === true ? <Spinner animation="border" variant="primary loading-page" /> : ""}
            <div className="container">
                <div className="timeline">
                    <div className="timeline-cover">

                        <div className="timeline-nav-bar ">
                            {/* hidden-sm hidden-xs */}
                            <div className="row">
                                <div className="col-md-3">
                                    <div className="profile-info">
                                        {datarender}
                                        <Button className="btn-upload" type="button" variant="link" onClick={() => setModalShow(true)} data-toggle="tooltip" data-placement="bottom" title="Upload Avatar"><i className="fas fa-camera"></i></Button>
                                        <ModalAvatar show={modalShow} userid={userid} onHide={() => setModalShow(false)} />
                                    </div>
                                </div>
                                <div className="col-md-9 d-flex justify-content-between"  >
                                    <ul className="list-inline profile-menu">
                                        <li><Link to={`/timeline/?userid=${data._id}`}>Timeline</Link></li>
                                        <li><Link to={`/timeline/about?userid=${data._id}`}>About</Link></li>
                                        <li><a href="timeline-album.html">Album</a></li>
                                        <li><a href="timeline-friends.html">Friends</a></li>
                                    </ul>
                                    {followresult ? followresult.map((item, index) => (
                                        <>
                                        
                                        <Link to={`/timeline?userid=${item._id}`} className={item._id===idaccount?"search-result-item__hidden":"search-result-item__addfriend mt-2 mr-4"} type="button" onClick={result?.includes(idaccount) === true ? (() => removeFollow(item._id,index)) : (() => addFollow(item._id,index))}><i className={`mr-2 ${result?.includes(idaccount) === true ? "fas fa-wifi" : "fas fa-user-plus"}`}></i>{result?.includes(idaccount) == true ? "Đang follow" : "Follow"}</Link>
                                   </>
                                    )):""

                                    }


                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="Image-cover">
                        {urlImagecover}

                    </div>
                    <div className="btn-uploadCoverImage">
                        <Button className="upload_cover" variant="link"
                            data-toggle="tooltip" data-placement="bottom" onClick={() => setshowcoverImg(true)}
                            title="Upload Image Cover">

                            <i className="fas fa-camera"></i>
                        </Button>
                        <ModalImageCover show={showcoverImg} userid={userid} onHide={() => setshowcoverImg(false)} />
                    </div>
                    <div>

                    </div>
                    <div id="page-contents">
                        <div className="row">
                            <div className="col-md-3 infor">
                                <div className="infor-user">
                                    <p className="infor-user__title"><i className="fas fa-globe-asia"></i> Giới thiệu</p>
                                    {checkstory.isStory === false ? StoryUser : InputStory}
                                    {checkstory.isStory === false ? <button className="infor-user__btn_edit" onClick={handleEditStory} type="button">Chỉnh sửa tiểu sử</button> : <div className="mt-3"><Button variant="light" onClick={handleExit}>Hủy</Button><Button className="ml-1" onClick={SubmitEditStory}>Lưu</Button></div>}

                                </div>
                                {inforUser}
                            </div>
                            <div className="col-md-7">
                                <Switch>
                                    <Route exact path="/timeline/" component={PostDetail} />
                                    <Route exact path="/timeline/about">
                                        <About data={data} />
                                    </Route>
                                </Switch>

                            </div>
                            <div className="col-md-2 static">
                                <div id="sticky-sidebar">
                                    <h4 className="grey">Sarah's activity</h4>
                                    <div className="feed-item">
                                        <div className="live-activity">
                                            <p><a href="#" className="profile-link">Sarah</a> Commended on a Photo</p>
                                            <p className="text-muted">5 mins ago</p>
                                        </div>
                                    </div>
                                    <div className="feed-item">
                                        <div className="live-activity">
                                            <p><a href="#" className="profile-link">Sarah</a> Has posted a photo</p>
                                            <p className="text-muted">an hour ago</p>
                                        </div>
                                    </div>
                                    <div className="feed-item">
                                        <div className="live-activity">
                                            <p><a href="#" className="profile-link">Sarah</a> Liked her friend's post</p>
                                            <p className="text-muted">4 hours ago</p>
                                        </div>
                                    </div>
                                    <div className="feed-item">
                                        <div className="live-activity">
                                            <p><a href="#" className="profile-link">Sarah</a> has shared an album</p>
                                            <p className="text-muted">a day ago</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}