import React, { useState, useEffect } from 'react';
// import {Img} from 'react-image'
// import logo from '/public/images/logo.png'
import './style.scss'
import { Navbar, NavDropdown, Nav, Form, Button, FormControl, InputGroup, Dropdown, DropdownButton } from 'react-bootstrap';
import { Link, Route, useHistory, Redirect } from 'react-router-dom';
import Switch from 'react-bootstrap/esm/Switch';
import PrivateRoute from '../Home/PrivateRoute';
import Login from '../Login/Login';
import Home from '../Home';
import { useDispatch, useSelector } from 'react-redux';
import { searchuser } from '../Home/alluser/action';
import io from 'socket.io-client';
const Header = () => {

    //   const listImage = [{
    //       imagUrl : '../../Asset/images/logo.jpg'
    //   }]
    const [search, setsearch] = useState('');
    const [issearch, setIssearch]= useState({ischeck: false})
    const history = useHistory();
    const dispatch = useDispatch();

    const isSearchs = useSelector(state => state.searchusers);
    
  
    // const socket = io('http://192.168.0.144:3002');
    const userid =localStorage.getItem('id')
    const signout = (e) => {
        if (window.confirm("Bạn có chắc muốn đăng xuất?")) {
            // socket.emit('logout',{user_id: userid});
            // socket.on('list-user',(dataAlluser)=>{
            //     console.log(dataAlluser);
            // })
            localStorage.removeItem("islogin");
            history.push("/login")
            
        }

    }
    const handleSearch = (e) =>{
          e.preventDefault();
          dispatch(searchuser(search))
          setTimeout(() => {
            history.push(`/search?q=${search}`);
          }, 1000);
    }
   
    return (

        <Navbar expand="lg" className="header">
            <Navbar className="container">
                <Navbar.Brand href="#home"><img src="/images/logo.png" /></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Form inline className="ml-auto position-relative" onSubmit={handleSearch}>
                        <InputGroup className="form-search">
                            
                            <Button variant="link p-0" type="submit"><i className="fas fa-search"></i></Button>
                            <FormControl id="inlineFormInputGroup" className="form-search_input" placeholder="Username" value={search} onChange={e => setsearch(e.target.value)}/>
                            
                        </InputGroup>
                        
                    </Form>
                    <Nav className="ml-auto menu">
                        <Link to="/" className="menu-link">Home</Link>
                        <NavDropdown title={<i className="fab fa-facebook-messenger"></i>} id="basic-nav-dropdown" size="xl">
                            hdhahd
                        </NavDropdown>
                        <NavDropdown title="Option" id="basic-nav-dropdown">
                            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item><Link to="/setting">Setting</Link></NavDropdown.Item>
                            <NavDropdown.Item><Button variant="link p-0 logout" onClick={signout}>Log out</Button></NavDropdown.Item>

                        </NavDropdown>

                    </Nav>
                    <Switch>
                        <Route exact path="/login">
                            <Login />
                        </Route>
                        <PrivateRoute path="/home" component={Home} />
                    </Switch>
                </Navbar.Collapse>
            </Navbar>



        </Navbar >

    )
};
export default Header;