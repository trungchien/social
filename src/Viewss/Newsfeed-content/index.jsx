import React, { useEffect } from 'react';
import '../Newsfeed-content/style.scss'
import { useDispatch, useSelector } from 'react-redux';
import { getuserid } from '../Timline/action';
import { useLocation } from 'react-router-dom';
export default function NewsfeedContent() {
    const userdata = useSelector(state => state.getusersID.listuserid)
   const dispatch = useDispatch();
//    const userid = localStorage.id
//    let query = new URLSearchParams(useLocation().search);
//     const userid = query.get("userid")
//    console.log(userid);
    console.log(userdata);
    var datarender = "";
    if (userdata)
        datarender = (
            <div>
                <a href="#"><img src={userdata.avatar !== "" ? userdata.avatar : userdata.avatar === "" && userdata.gender === 1 ? "/images/users/male.jpg" : "/images/users/female.jpg"} className="profile-photo" /></a>
                
            </div>
        )
    else datarender = <></>
    return (
        <div className="p-0">
            <div className="create-post">
                <div className="row">
                    <div className="col-md-7 col-sm-7">
                        <div className="form-group">
                            {datarender}
                            <textarea name="texts" id="exampleTextarea" cols={30} rows={1} className="form-control ml-3" placeholder="Write what you wish" defaultValue={""} />
                        </div>
                    </div>
                    <div className="col-md-5 col-sm-5">
                        <div className="tools">
                            <ul className="publishing-tools list-inline">
                                <li><a href="#"><i className="ion-compose" /></a></li>
                                <li><a href="#"><i className="ion-images" /></a></li>
                                <li><a href="#"><i className="ion-ios-videocam" /></a></li>
                                <li><a href="#"><i className="ion-map" /></a></li>
                            </ul>
                            <button className="btn btn-primary pull-right">Publish</button>
                        </div>
                    </div>
                </div>
            </div>{/* Post Create Box End*/}
            {/* Post Content
      ================================================= */}
            <div className="post-content">
                <img src="images/post-images/1.jpg" className="img-responsive post-image" />
                <div className="post-container d-flex">
                    <img src="images/users/user-5.jpg" className="profile-photo-md pull-left" />
                    <div className="post-detail">
                        <div className="user-info">
                            <h5><a href="timeline.html" className="profile-link">Alexis Clark</a> <span className="following">following</span></h5>
                            <p className="text-muted">Published a photo about 3 mins ago</p>
                        </div>
                        <div className="reaction">
                            <a className="btn text-green"><i className="icon ion-thumbsup" /> 13</a>
                            <a className="btn text-red"><i className="fa fa-thumbs-down" /> 0</a>
                        </div>
                        <div className="line-divider" />
                        <div className="post-text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. <i className="em em-anguished" /> <i className="em em-anguished" /> <i className="em em-anguished" /></p>
                        </div>
                        <div className="line-divider" />
                        <div className="post-comment">
                            <img src="images/users/user-11.jpg" className="profile-photo-md" />
                            <p><a href="timeline.html" className="profile-link">Diana </a><i className="em em-laughing" /> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </p>
                        </div>
                        <div className="post-comment">
                            <img src="images/users/user-4.jpg"  className="profile-photo-md" />
                            <p><a href="timeline.html" className="profile-link">John</a> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud </p>
                        </div>
                        <div className="post-comment">
                            <img src="images/users/user-1.jpg"  className="profile-photo-md" />
                            <input type="text" className="form-control" placeholder="Post a comment" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    )
}