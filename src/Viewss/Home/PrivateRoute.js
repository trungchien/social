import React, { useEffect } from "react";
import { Route, Redirect, useLocation, useHistory } from "react-router-dom";
import Login from "../Login/Login";



function PrivateRoute({ component: Component, ...rest }) {
  const islogin = localStorage.getItem('islogin');

  const location = useLocation();
  const history = useHistory();

  return (
    <>
    <Route
      {...rest}
      render={props =>
        islogin ? (
          <Component {...props} />
        ) : (
          <Redirect to="/login"/>
        )
      }
    />
 
      </>
     
  //    <Route {...rest} render={props => {
  //     const currentUser = authenticationService.currentUserValue;
  //     if (!currentUser) {
  //         // not logged in so redirect to login page with the return url
  //         return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
  //     }

  //     // authorised so return component
  //     return <Component {...props} />
  // }} />
    
    
  );
}

export default PrivateRoute;