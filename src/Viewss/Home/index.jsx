import React, { useEffect, Fragment, useState, useRef } from 'react';
import Login from '../Login/Login';
import Header from '../Header';
import MenuNewsfeed from '../Newsfeed-left';
import NewsfeedContent from '../Newsfeed-content';
import { Route, Link, useLocation, useHistory, Redirect } from 'react-router-dom';
import Switch from 'react-bootstrap/esm/Switch';
import Message from '../Message';
import { useSelector, useDispatch } from 'react-redux';
import Timeline from '../Timline';
import People from './People';
import jwt_decode from 'jwt-decode';
import { getuserid } from '../Timline/action';
import { Spinner, Navbar } from 'react-bootstrap';
import setting from './Setting';
import { getalluser } from './alluser/action';
import Search from './SearchSuccess';
import io from 'socket.io-client';

const Home = (props) => {
    const number = 1;
    const [page,setPage] = useState(number)
    const [datalist,setDatalist] = useState([])
    const token = localStorage.getItem('islogin');
    const usernamess = localStorage.getItem('username')
    const pagenext = useSelector(state => state.getallusers.page);
    const pagepre = useSelector(state => state.getallusers.pagepre);
    console.log(pagenext);
    console.log(pagepre);
    // let query = new URLSearchParams(useLocation().search);
    const history = useHistory();
    const dispatch = useDispatch();
    var decoded = jwt_decode(token);
    
    // const socket = io('http://192.168.0.144:3002');
    useEffect(() => {
        dispatch(getuserid(decoded.userId));
        // dispatch(getalluser(page));
        // socket.emit('online',{user_id: decoded.userId});
        // socket.on('list-user',(dataAlluser)=>{
        //     setDatalist(dataAlluser)
        //  })
    }, [])
   const handleScroll = (e) => {
       
        const bottom = Number((e.target.scrollHeight - e.target.scrollTop).toFixed(0)) - e.target.clientHeight < 10;
        const top = Number((e.target.scrollHeight - e.target.scrollTop).toFixed(0))- e.target.clientHeight > 50  ;
        console.log(top);
        if (bottom) {
            if(pagenext>0){
                dispatch(getalluser(pagenext))
                e.target.scrollTop = 70;
                
            }
        } 
        else if(top){
            if(pagepre>0){
                dispatch(getalluser(pagepre))
                
                
            } 
        }   
    };
    const data = useSelector(state => state.getusersID.listuserid);

    const Alluser = useSelector(state => state.getallusers.listuser.docs)
   console.log(Alluser);
  

    // Search_success
    const list = document.getElementById('listuser');
    
    const loadingpage = useSelector(state => state.getusersID)
    var datauser = "";
    if (data) {
        datauser = (
            <>
                <Link to={`/timeline?userid=${data._id}`} className="text-white">
                    <img src={data.avatar !== "" ? data.avatar : data.avatar === "" && data.gender === 1 ? "/images/users/male.jpg" : "/images/users/female.jpg"} className="profile-photo" />
                    <span className="username_infor">{data.firstName} {data.lastName}</span>

                </Link>
                <br></br>
                <a href="#" className="text-white">1222 Follower</a>
            </>
        )
    }
    return (

        <div>

            <Header />
            {loadingpage.loadpage === true ? (<Spinner animation="border" variant="primary loading-page" />) : (<></>)}
            <div id="page-contents" className={loadingpage.loadpage === true ?"page-hidden":""}>

                <div className="container">
                    <div className="row">

                        <div className="col-md-3">
                            <div className="profile-card mt-4">
                                {datauser}
                            </div>
                            <ul className="nav-news-feed">
                                <li><i className="icon ion-ios-paper" /><div><Link to={`/?userid=${data._id}`}>My Newsfeed</Link></div></li>
                                <li><i className="icon ion-ios-people" /><div><Link to="/people">People Nearby</Link></div></li>
                                <li><i className="icon ion-ios-people-outline" /><div><a href="newsfeed-friends.html">Friends</a></div></li>
                                <li><i className="icon ion-chatboxes" /><div><Link to="/message">Message</Link> </div></li>
                                <li><i className="icon ion-images" /><div><a href="newsfeed-images.html">Images</a></div></li>
                                <li><i className="icon ion-ios-videocam" /><div><a href="newsfeed-videos.html">Videos</a></div></li>
                            </ul>
                            <div id="chat-block">

                                <div className="title">Chat online</div>

                            </div>
                        </div>
                        <div className="col-md-7">
                            <div>
                            
                                <Switch>
                                    <Route path="/people" component={People} />
                                    <Route exact path="/">
                                        <NewsfeedContent />
                                    </Route>
                                    <Route path="/message">
                                        <Message />
                                    </Route>
                                </Switch>

                            </div>

                        </div>
                        <div className="col-md-2 listuser" >
                            <h3 className="listuser-title">Danh bạ</h3>
                            <div className="listuser-description" id="listuser" onScroll={handleScroll}>
                                {datalist ? datalist.map((item,index) => (
                                     
                                    <div className="listuser-description__sub" key={index}>
                                        {/* {item?.followers?.includes(decoded.userId) === true ? */}
                                        <Link to={`/timeline?userid=${item._id}`} className="d-flex">
                                            <img src={item.avatar !== "" ? item.avatar : item.avatar === "" && item.gender === 1 ? "/images/users/male.jpg" : "/images/users/female.jpg"} className="profile-photo" />
                                            <span className="username_infor">{item.firstName} {item.lastName}</span>

                                        </Link>
                                        {/* } */}
                                    </div>

                                )) : ""}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>



    )
}
export default Home;