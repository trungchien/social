import React from 'react';
import Header from '../../Header';
import { Link, Route } from 'react-router-dom';
import Switch from 'react-bootstrap/esm/Switch';
import { ChangePassword } from './ChangePassword';
const setting = () => {
    return (
        <div>
            <Header />
            <div className="container">
                <div className="row">

                    <div className="col-md-3">
                       
                        <ul className="nav-news-feed">
                            <li><i className="icon ion-ios-paper" /><div><Link to="/">Change Password</Link></div></li>
                            <li><i className="icon ion-ios-people" /><div><Link to="/people">People Nearby</Link></div></li>
                            <li><i className="icon ion-ios-people-outline" /><div><a href="newsfeed-friends.html">Friends</a></div></li>
                            <li><i className="icon ion-chatboxes" /><div><Link to="/message">Message</Link> </div></li>
                            <li><i className="icon ion-images" /><div><a href="newsfeed-images.html">Images</a></div></li>
                            <li><i className="icon ion-ios-videocam" /><div><a href="newsfeed-videos.html">Videos</a></div></li>
                        </ul>
                    </div>
                    <div className="col-md-9">
                        <Switch>
                            <Route path="/" component={ChangePassword}/>
                        </Switch>
                    </div>
                </div>
            </div>
        </div>

    )
}
export default setting;