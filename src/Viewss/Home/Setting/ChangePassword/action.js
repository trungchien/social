export const ActionChangePassword = {
   CHANGE_PASSWORD: 'CHANGE_PASSWORD',
   CHANGE_PASSWORD_SUCCESS: 'CHANGE_PASSWORD_SUCCESS',
   CHANGE_PASSWORD_ERROR: 'CHANGE_PASSWORD_ERROR'
}
export const changepass = (pass, newpass) => {
   return {
       type: ActionChangePassword.CHANGE_PASSWORD,
       payload: {
           pass,
           newpass
       }
   }
}

export const changepasssuccess = (list) => {
    return {
        type: ActionChangePassword.CHANGE_PASSWORD_SUCCESS,
        payload: list
    }
 }

 export const changepasserror = (list) => {
    return {
        type: ActionChangePassword.CHANGE_PASSWORD_ERROR,
        payload: list
    }
 }