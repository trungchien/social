import React, { useState } from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { changepass } from './action';
export const ChangePassword = () => {
    const [password,setpassword] = useState('');
    const [newpassword, setnewpassword] = useState('');
    const [confirmpassword ,setconfirmpassword] = useState('');
    const dispatch = useDispatch();
    const handlechange = () => {
        if(password, newpassword === confirmpassword){
            dispatch(changepass(password,newpassword))
        }
      
    }
 return (
     <div>
         <h3>Change Password cho User</h3>
         <Form className="mt-5">
                <Form.Group as={Row} controlId="formPlaintextEmail">
                    <Form.Label column sm="2">
                       Mật khẩu cũ
                    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setpassword(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPlaintextEmail">
                    <Form.Label column sm="2">
                       Mật khẩu mới
                    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setnewpassword(e.target.value)} />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="formPlaintextEmail">
                    <Form.Label column sm="2">
                       Nhập lại khẩu mới
                    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setconfirmpassword(e.target.value)} />
                    </Col>
                </Form.Group>
                <Button variant="primary" onClick={handlechange}>Send Forgot</Button>
                
            </Form>
     </div>
 )
}