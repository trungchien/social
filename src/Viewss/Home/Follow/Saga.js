import { takeLatest, put } from "redux-saga/effects";
import { ActionType_Follow } from "./action";
import { getuserid } from "../../Timline/action";
import { searchuser, searchusersuccess } from "../alluser/action";
function* SagaFollow(id){
    console.log(id.payload);
    const token = localStorage.getItem('islogin');
    console.log(token);
    try {
        const reqfollow = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/follow-user/${id.payload}`,{
            method: 'PUT',
            headers: new Headers({
                'Accept' : '*/*',
                'Authorization': `Bearer ${token}`
                
            })
        })
        const resfollow = yield reqfollow.json();
       
        console.log(resfollow);
    } catch (error) {
        
    }
}
function* SagaunFollow(id){
    const token = localStorage.getItem('islogin');
  
    try {
        const requnfollow = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/unfollow-user/${id.payload}`,{
            method: 'PUT',
            headers: new Headers({
                'Accept' : '*/*',
                'Authorization': `Bearer ${token}`
                
            })
        })
        const resunfollow = yield requnfollow.json();
       
    } catch (error) {
        
    }
}
export function* WatchSagaFollow(){
    yield takeLatest(ActionType_Follow.FOLLOW, SagaFollow)
    yield takeLatest(ActionType_Follow.UNFOLLOW, SagaunFollow)
}