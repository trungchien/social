export const ActionType_Follow = {
    FOLLOW: 'FOLLOW',
    FOLLOW_SUCCESS: 'FOLLOW_SUCCESS',
    FOLLOW_ERROR: 'FOLLOW_ERROR',

    UNFOLLOW: 'UNFOLLOW',
    UNFOLLOW_SUCCESS: 'UNFOLLOW_SUCCESS',
    UNFOLLOW_ERROR: 'UNFOLLOW_ERROR'
}

export const follow = (data) => {
    return {
        type: ActionType_Follow.FOLLOW,
        payload: data
    }
}

export const followsuccess = (data) => {
    return {
        type: ActionType_Follow.FOLLOW_SUCCESS,
        payload: data
    }
}

export const followerror = (data) => {
    return {
        type: ActionType_Follow.FOLLOW_ERROR,
        payload: data
    }
}

export const unfollow = (data) => {
    return {
        type: ActionType_Follow.UNFOLLOW,
        payload: data
    }
}

export const unfollowsuccess = (data) => {
    return {
        type: ActionType_Follow.UNFOLLOW_SUCCESS,
        payload: data
    }
}

export const unfollower = (data) => {
    return {
        type: ActionType_Follow.UNFOLLOW_ERROR,
        payload: data
    }
}