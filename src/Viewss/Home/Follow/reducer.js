import { ActionType_Follow } from "./action";

const stateFollow = {
    follow: false
}
const followreducer = (state = stateFollow, action) => {
    switch (action.type) {
        case ActionType_Follow.FOLLOW: {
            return { ...state,follow:true }
        }
        case ActionType_Follow.FOLLOW_SUCCESS: {
            return { ...state }
        }
        case ActionType_Follow.FOLLOW_ERROR: {
            return { ...state }
        }
        case ActionType_Follow.UNFOLLOW: {
            return { ...state,follow:false }
        }
        case ActionType_Follow.UNFOLLOW_SUCCESS: {
            return { ...state }
        }
        case ActionType_Follow.UNFOLLOW_ERROR: {
            return { ...state }
        }
        default:
            return {...state}
    }
}

export default followreducer;