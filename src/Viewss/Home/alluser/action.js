export const Action_Type_Getalluser = {
    GET_ALLUSER: 'GET_ALLUSER',
    GET_ALLUSER_SUCCESS: 'GET_ALLUSER_SUCCESS',
    GET_ALLUSER_ERROR: 'GET_ALLUSER_ERROR',

    SEARCH_USER : 'SEARCH_USER',
    SEARCH_USER_SUCCESS: 'SEARCH_USER_SUCCESS',
    SEARCH_USER_ERROR: 'SEARCH_USER_ERROR'
}
export const getalluser = (list) => {
    return {
        type: Action_Type_Getalluser.GET_ALLUSER,
        payload: list
    }
}

export const getallusersuccess = (list) => {
    return {
        type: Action_Type_Getalluser.GET_ALLUSER_SUCCESS,
        payload: list
    }
}

export const getallusererror = (list) => {
    return {
        type: Action_Type_Getalluser.GET_ALLUSER_ERROR,
        payload: list
    }
}

export const searchuser = (list) => {
    return {
        type: Action_Type_Getalluser.SEARCH_USER,
        payload: list
    }
}

export const searchusersuccess = (list) => {
    return {
        type: Action_Type_Getalluser.SEARCH_USER_SUCCESS,
        payload: list
    }
}

export const searchusererror = (list) => {
    return {
        type: Action_Type_Getalluser.SEARCH_USER_ERROR,
        payload: list
    }
}
