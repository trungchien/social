const { Action_Type_Getalluser } = require("./action");

const user = {

    listuser: [],
    page: '',
    pagepre:''
}
const getallusers = (state = user, action) => {
    switch (action.type) {
        case Action_Type_Getalluser.GET_ALLUSER:{
            return {...state}
        }
        case Action_Type_Getalluser.GET_ALLUSER_SUCCESS:{
            console.log(action.payload);
            return {...state, listuser: action.payload,page: action.payload.nextPage,pagepre: action.payload.prevPage}
        }  
        case Action_Type_Getalluser.GET_ALLUSER_ERROR:{
            return {...state}
        }  
        default:
            return{
                ...state
            };
    }
};
export default getallusers;