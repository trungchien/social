const { Action_Type_Getalluser } = require("./action");

const user = {
    listuser: [],
    isSearch: false,
    
}
const searchusers = (state = user, action) => {
    switch (action.type) {
        case Action_Type_Getalluser.SEARCH_USER : {
            return {...state}
        }
        case Action_Type_Getalluser.SEARCH_USER_SUCCESS : {
            console.log(action.payload);
            return {...state,listuser: action.payload}
        }
        case Action_Type_Getalluser.SEARCH_USER_ERROR : {
            return {...state}
        }
        default:
            return{
                ...state
            };
    }
};
export default searchusers;