import { takeLatest, put } from "redux-saga/effects";
import { Action_Type_Getalluser, getallusersuccess, getallusererror, searchusersuccess, searchusererror } from "./action";
import axios from "axios";


function* Sagagetalluser(page) {
    const token = localStorage.getItem('islogin');
    // const user= useSelector(state => state.loginss.lists)
    // console.log(user);
    // console.log(token);
    
    if (token)
        try {
            const reqgetuser = yield fetch(`http://905154fe813c.ngrok.io/api/v1/user/get-all-user?page=${page.payload}`, {
                method: "GET",
                headers: new Headers({
                    'Content-Type': 'application/json',
                    Accept: 'application/json',
                    'Authorization': `Bearer ${token}`
                })
            })
            const resgetUser = yield reqgetuser.json();
            console.log(resgetUser);
            yield put(getallusersuccess(resgetUser.data));
        }

        catch (error) {
            yield put(getallusererror(error));
        }
}
function* SagaSearchuser(data){
    console.log(data.payload);
    const token = localStorage.getItem('islogin');

    
           if(token)
           try {
            const reqgetuser = yield fetch(`https://social-aht.herokuapp.com/api/v1/user/search?q=${data.payload}`, {
              
                headers: new Headers({
                  
                    'Accept': '*/*',
                    
                    'Content-Type': 'text/html; charset=utf-8',
                    'Access-Control-Allow-Origin' : '*/*',
                    'Authorization': `Bearer ${token}`
                }),

            })
            const resgetUser = yield reqgetuser.json();

            console.log(resgetUser);
            if(resgetUser.state === true){
                yield put(searchusersuccess(resgetUser.data));
            }
           } catch (error) {
               yield put(searchusererror(error))
           }
            
}
export default function* watchSagaAlluser() {
    yield takeLatest(Action_Type_Getalluser.GET_ALLUSER, Sagagetalluser)
    yield takeLatest(Action_Type_Getalluser.SEARCH_USER, SagaSearchuser)
}