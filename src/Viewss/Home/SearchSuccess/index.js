
import React, { useState, useEffect } from 'react';
import Header from "../../Header/index.jsx";
import { useSelector, useDispatch } from "react-redux";
import '../SearchSuccess/style.scss'
import { Link, useLocation } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { follow, unfollow } from '../Follow/action.js';
import { getuserid } from '../../Timline/action.js';
const Search = () => {
    const [follows,setfollows] = useState(false);
    const dispatch = useDispatch();
    const search_success = useSelector(state => state.searchusers.listuser);
    const idaccountSearch = search_success.map(item=>item._id);
    console.log(idaccountSearch);
    const idresultSearch = search_success.map(item=>item.followers)
   
    // const isfollow = useSelector(state => state.followreducer.follow);
    console.log(idresultSearch);
    
    const idaccount = localStorage.getItem('id')
    let query = new URLSearchParams(useLocation().search);
    const keysearch = query.get("q")
    
  const addFollow = (id,index) => {
        if (id) {
            dispatch(follow(id))
           
        }
        idresultSearch[index].push(idaccount);
    }
    const removeFollow = (id,index) => {
        if (id) {
            dispatch(unfollow(id))
        }
    
        idresultSearch[index].splice(idresultSearch[index].indexOf(idaccount), 1);
    }
  
    return (
        <div className="search-page">
            <Header />
            <div className="container mt-4">
                <div className="row">

                    <div className="col-12 col-sm-12 col-md-5 col-xl-5 col-lg-5 px-0 search-result">
                        <h5>Kết quả tìm kiếm dành cho bạn </h5>
                        {search_success.map((item,index) => (
                            <div className="col-12 d-flex justify-content-between search-result-item mb-3">
                                <Link to={`/timeline?userid=${item._id}`} className="d-flex">
                                    <img src={item.avatar !== "" ? item.avatar : item.avatar === "" && item.gender === 1 ? "/images/users/male.jpg" : "/images/users/female.jpg"} className="profile-photo" />
                                    <span className="username_infor">{item.firstName} {item.lastName}</span>
                                   
                                </Link>
                                
                        <Link to={`/search?q=${keysearch}`}  className={item._id===idaccount?"search-result-item__hidden":"search-result-item__addfriend"} type="button" onClick={idresultSearch[index].includes(idaccount) === true ? (()=>removeFollow(item._id,index)) :(()=>addFollow(item._id,index))}><i className={`mr-2 ${idresultSearch[index].includes(idaccount) === true ? "fas fa-wifi" : "fas fa-user-plus"}`}></i>{idresultSearch[index].includes(idaccount) === true ? "Đang follow" : "Follow" }</Link>
                            </div>
                        ))}

                    </div>
                    <div className="col-hidden sm-hidden md-7 col-xl-7 col-lg-7">

                    </div>
                </div>
            </div>
        </div>

    )
}
export default Search;