import React, { useState, useEffect } from 'react';
import '../Login/style.scss';
import { Switch, useHistory, Route, useLocation, Redirect, Link } from 'react-router-dom';
import Home from '../Home';
import { useDispatch, useSelector } from 'react-redux';
import { login } from './action';
import { Signup } from '../sign_up';
import { Spinner } from 'react-bootstrap';
const Login = () => {
    const [usernames, setUsername] = useState('');
    const [passwords, setPassword] = useState('');
    
    const dispatch = useDispatch();
    const islogin = localStorage.getItem('islogin');
    const history = useHistory();
    const [signup , setSignup] = useState('')

    const tokenlogin = useSelector(state => state.loginss)
    
    // console.log(islogin);
    // let history = useHistory();
    let location = useLocation();
    let { from } = location.state || { from: { pathname: "/home" } };
    const loginuser = useSelector(state => state.loginss)
console.log(loginuser.isError);
    
    
    const handlesubmit = (e) => {
        e.preventDefault();
        if(usernames === "",passwords=== ""){
            document.getElementById("inputnull").innerHTML = "(*) Bạn chưa nhập tài khoản hoặc mật khẩu"
        }
             
        else{
            dispatch(login({ username: usernames, password: passwords }));
            
        }

       
        
        
        // if (username === users.name && users.pass) {
        //     history.replace(from)
        //     localStorage.setItem('islogin', 'true')
        // }
        // if(islogin === true){
        //     history.replace(from)
        //     console.log('kkk');
        //    }
    }
       
    if(loginuser.isError !== ''){
        document.getElementById("inputnull").innerHTML = "Bạn nhập sai tài khoản hoặc mật khẩu";
   }
    if (islogin) {
        alert('Đăng nhập thành công')
        
        return <Redirect to="/" />
        
    }
    const handlesignup = () => {
    //   document.getElementById("inputnull").innerHTML = "_";
      history.push('/signup')
    }
    
    return (
        <div className="limiter">
            <div className="container-login100">
                <div className="wrap-login100">
                  
                    <form className="login100-form validate-form" onSubmit={handlesubmit}>
                        <span className="login100-form-title mb-5">
                            Login to continue
                        </span>
                        <div className="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                            <input className="input100" type="text" name="email" onChange={(e) => setUsername(e.target.value)} placeholder="Email" />
                            <span className="focus-input100" />

                        </div>
                        <div className="wrap-input100 validate-input" data-validate="Password is required">
                            <input className="input100" type="password" name="pass" onChange={(e) => setPassword(e.target.value)} placeholder="Password" />
                            <span className="focus-input100" />

                        </div>
                        <div className="d-flex justify-content-between w-full pt-3 pb-3">
                            <div className="contact100-form-checkbox">
                                <input className="input-checkbox100" id="ckb1" type="checkbox" name="remember-me" />
                                <label className="label-checkbox100" htmlFor="ckb1">
                                    Remember me
                                </label>
                            </div>
                            <div>
                                <Link to="/forgot-password" className="txt1">
                                    Forgot Password?
                                </Link>
                            </div>
                            
                        </div>
                        <p id="inputnull" className="inputnull">Bạn vui lòng nhập tài khoản và mật khẩu</p>
                        <div className="container-login100-form-btn mt-3">
                            <button className="login100-form-btn" type="submit">
                                Login
                            </button>
                            {tokenlogin.isloading === true ?(<Spinner animation="border" variant="primary" />):(<></>)}
                        </div>
                        <div className="text-center p-t-46 p-b-20 mt-3">
                            <span className="txt2">
                                or sign up using
                            </span>
                        </div>
                        <div className="login100-form-social d-lex text-center mt-3">
                            <a href="#" className="login100-form-social-item">
                                <i className="fab fa-facebook-f"></i>
                            </a>
                            <a href="#" className="login100-form-social-item ">
                                <i className="fab fa-twitter"></i>
                            </a>
                        </div>
                        <div className="container-login100-form-btn mt-4">
                            <button className="login100-form-btn sign-up" type="submit" onClick={handlesignup}>
                                Sign up
                            </button>
                        </div>
                    </form>
                    <div className="login100-more">
                    </div>
                </div>
            </div>
        </div>
    )
};
export default Login;