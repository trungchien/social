import { Form, Button, Col, Row } from "react-bootstrap";
import React, { useState } from 'react';
import { useDispatch } from "react-redux";
import { forget } from "../sign_up/action";
import { useHistory } from "react-router-dom";
const ForgotPassword = () => {
    const [forgot,setforgot] = useState('');

    const dispatch = useDispatch();
    const history = useHistory();
    const handleforgot = ()=>{
        if(forgot){
             dispatch(forget(forgot))
             history.push("/login")
        }
       
    }
    const backlogin= () => {
        history.push("/login")
    }
    return (
        <div>
            <h1 className="text-center">Forgot Password</h1>
            <div className=" container">
           

            <Form className="container mt-5 w-50">
                <Form.Group as={Row} controlId="formPlaintextEmail">
                    <Form.Label column sm="2">
                       Email
    </Form.Label>
                    <Col sm="10">
                        <Form.Control type="text" onChange={e => setforgot(e.target.value)} />
                    </Col>
                </Form.Group>
                
                <Button variant="primary" onClick={handleforgot}>Send Forgot</Button>
                <Button variant="danger ml-3" onClick={backlogin}>Close</Button>
            </Form>
        </div>
        </div>
    )
}
export default ForgotPassword;