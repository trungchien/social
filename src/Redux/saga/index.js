import { all } from 'redux-saga/effects';
import watchsagaitem from '../../Viewss/Login/saga';
import watchSagasignup from '../../Viewss/sign_up/saga'
import watchSagauserid from '../../Viewss/Timline/saga';
import watchSagaUploadAvt from '../../Viewss/Timline/Modal-avatar/saga';
import WatchSagaChangePassword from '../../Viewss/Home/Setting/ChangePassword/saga';
import watchSagaAlluser from '../../Viewss/Home/alluser/saga';
import { WatchSagaFollow } from '../../Viewss/Home/Follow/Saga';

function* rootSaga() {
    yield all([
        watchsagaitem(),
        watchSagasignup(),
        watchSagauserid(),
        watchSagaUploadAvt(),
        WatchSagaChangePassword(),
        watchSagaAlluser(),
        WatchSagaFollow()
    ]
    )
};
export default rootSaga;