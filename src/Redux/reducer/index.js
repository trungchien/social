
import loginss from "../../Viewss/Login/reducer";

import signupreducer from "../../Viewss/sign_up/reducer";
import getusersID from "../../Viewss/Timline/reducer";
import uploadavatarss from "../../Viewss/Timline/Modal-avatar/reducer";
import getallusers from "../../Viewss/Home/alluser/reducer";
import searchusers from "../../Viewss/Home/alluser/reducer_search";
import followreducer from "../../Viewss/Home/Follow/reducer";

const { combineReducers } = require("redux");

const rootReducer = combineReducers({
    loginss: loginss,
    signupreducer: signupreducer,
    getusersID: getusersID,
    uploadavatarss: uploadavatarss,
    getallusers: getallusers,
    searchusers: searchusers,
    followreducer: followreducer
});
export default rootReducer;