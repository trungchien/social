import React, { useState, useEffect } from 'react';
import './App.css';
import './style.scss';
import Login from './Viewss/Login/Login';
import Home from './Viewss/Home/index.jsx';
import { Route, Switch, BrowserRouter as Router, Link } from 'react-router-dom';

import PrivateRoute from './Viewss/Home/PrivateRoute';


import { Signup } from './Viewss/sign_up';
import Timeline from './Viewss/Timline';
import ForgotPassword from './Viewss/Forgot_Passwork';
import setting from './Viewss/Home/Setting';
import Search from './Viewss/Home/SearchSuccess';
// import io from 'socket.io-client';

function App() {
  // let socket = io('https://social-aht.herokuapp.com',{ transports: ['xhr-polling'] });
  // useEffect(()=>{
  //   socket.emit('online',{user_id: '5f34dc7b74c12100043e0506'});
  //   socket.on('list-user',(datass)=>{
     
  //        console.log({datass});
  //    })
  // },[])
 
  
  return (

  <Router>
        <Switch>
        <Route path="/login" component={Login}/>
        <Route path="/signup" component={Signup}/>
        
        <PrivateRoute path="/setting" component={setting}/>
           
        <Route path = "/forgot-password" component={ForgotPassword} />
        <PrivateRoute  path ="/search" component={Search} />
        <PrivateRoute  path ="/timeline" component={Timeline} />
        
        <PrivateRoute  path ="/" component={Home} />
        
        </Switch>
    </Router>
  );
}

export default App;
